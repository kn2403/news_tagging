import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer
from pprint import pprint
from nltk.stem.snowball import SnowballStemmer
import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.preprocessing import MultiLabelBinarizer
import os
import numpy as np
import scipy.io
import pickle

#Extract features from text
cpath = os.getcwd()
text_path = os.path.join(cpath, 'data', 'interim', 'text_clean.txt')

text_clean = []
with open(text_path, "r") as f:

    for line in f.readlines():
        text_clean.append(line.strip())

n = int(len(text_clean) * 0.2)

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(text_clean)
tfidf_transformer = TfidfTransformer()
X_tfidf = tfidf_transformer.fit_transform(X_train_counts)

features_path1 = os.path.join(cpath, 'data', 'processed', 'features_train.mtx')
features_path2 = os.path.join(cpath, 'data', 'processed', 'features_test.mtx')
scipy.io.mmwrite(features_path1, X_tfidf[:-n])
scipy.io.mmwrite(features_path2, X_tfidf[-n:])

#Extract features from tags
cpath = os.getcwd()
tags_path = os.path.join(cpath, 'data', 'interim', 'tags_clean.txt')
tags_clean = []
with open(tags_path, "r") as f:

    for line in f.readlines():
        tags_clean.append(line.strip().split(','))

mlb = MultiLabelBinarizer()
tags = mlb.fit_transform(tags_clean)
class_path = os.path.join(cpath, 'data', 'interim', 'classes.pkl')
pickle.dump(mlb, open(class_path, 'wb'))

#save the tags
tag_path1 = os.path.join(cpath, 'data', 'processed', 'tags_train.txt')
np.savetxt(tag_path1, tags[:-n])
tag_path2 = os.path.join(cpath, 'data', 'processed', 'tags_test.txt')
np.savetxt(tag_path2, tags[-n:])
