import os
import sklearn
import numpy as np
from sklearn.metrics import coverage_error, label_ranking_average_precision_score, label_ranking_loss
import pickle
from scipy import io
import argparse

#exist_models
cpath = os.getcwd()
path = os.path.join(cpath, 'models', 'method1')
exist_models = []
for file in os.listdir(path):
    if file.endswith(".sav"):
        exist_models.append(file.split('_')[0])

parser = argparse.ArgumentParser()
parser.add_argument('--LinearSVC', action='store_true')
parser.add_argument('--DecisionTree', action='store_true')
parser.add_argument('--RandomForest', action='store_true')
parser.add_argument('--KNN', action='store_true')
parser.add_argument('--NaiveBayes', action='store_true')
input_args = vars(parser.parse_args())
pars = dict((k, v) for k, v in input_args.items() if v == True)
alg = list(pars.keys())

if alg == []:
    alg = exist_models
elif any(a not in exist_models for a in alg):
    print('Please train the model first!')
    exit()

#Load test data
tag_path = os.path.join(cpath, 'data', 'processed', 'tags_test.txt')
Y_test = np.loadtxt(tag_path)
text_path = os.path.join(cpath, 'data', 'processed', 'features_test.mtx')
X_test = io.mmread(text_path)
X_test = X_test.tocsr()


#test function: arg as a str
def Test_Model(arg, X_test=X_test, Y_test=Y_test, cpath=cpath):

    model_path = os.path.join(cpath, 'models','method1', '%s_model.sav' % arg)
    loaded_model = pickle.load(open(model_path, 'rb'))

    scores_path = os.path.join(cpath, 'models', 'method1','%s_CVscores.pkl' % arg)
    CVscores = pickle.load(open(scores_path, 'rb'))

    Testscores = loaded_model.score(X_test, Y_test)
    predicted = loaded_model.predict(X_test)
    cov_error = coverage_error(Y_test, predicted)
    label_error = label_ranking_average_precision_score(Y_test, predicted)
    rank_error = label_ranking_loss(Y_test, predicted)

    results = [arg, CVscores, Testscores, cov_error, label_error, rank_error]

    return results


#Get results
test_results = []

for a in alg:
    r = Test_Model(a)
    test_results.append(r)

filename = os.path.join(cpath, 'data', 'interim', 'test_results.pkl')
pickle.dump(test_results, open(filename, 'wb'))
