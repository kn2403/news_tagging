import os
import sklearn
import pandas as pd
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsRestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import KFold, cross_val_score
import statistics as s
import pickle
from scipy import io
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--LinearSVC', action='store', type=float, nargs='+')
parser.add_argument('--DecisionTree', action='store', type=int, nargs='+')
parser.add_argument('--RandomForest', action='store', type=int, nargs='+')
parser.add_argument('--KNN', action='store', type=int, nargs='+')
parser.add_argument('--NaiveBayes', action='store', type=float, nargs='+')
input_args = vars(parser.parse_args())
pars = dict((k, v) for k, v in input_args.items() if v)
alg = list(pars.keys())

if not pars:
    svc_par = [str(x) for x in np.logspace(-6, -1, 10).tolist()]
    pars = {
        "LinearSVC": svc_par,
        "DecisionTree": [1, 2, 3, 4, 5, 6, 7, 8],
        "RandomForest": [10, 15, 20, 25, 30],
        "KNN": [5, 7, 10, 15, 20],
        "NaiveBayes": [0.2, 0.5, 0.7, 1]
    }
    alg = list(pars.keys())

#Training function
cpath = os.getcwd()
tag_path = os.path.join(cpath, 'data', 'processed', 'tags_train.txt')
Y_train = np.loadtxt(tag_path)
text_path = os.path.join(cpath, 'data', 'processed', 'features_train.mtx')
X_train = io.mmread(text_path)
X_train = X_train.tocsr()


def Train_Model(alg, pars, X_train=X_train, Y_train=Y_train, cpath=cpath):

    scores = []
    #Cross Validations for the selected model
    for c in pars:
        models = {
            "LinearSVC": LinearSVC(class_weight="balanced", C=c),
            "DecisionTree": DecisionTreeClassifier(max_depth=c),
            "RandomForest": RandomForestClassifier(max_features=c),
            "KNN": KNeighborsClassifier(n_neighbors=c),
            "NaiveBayes": MultinomialNB(alpha=c)
        }
        classifier = OneVsRestClassifier(models[alg])
        k_fold = KFold(n_splits=5)
        scores_list = [
            classifier.fit(X_train[train], Y_train[train]).score(
                X_train[test], Y_train[test])
            for train, test in k_fold.split(X_train)
        ]
        scores.append(s.mean(scores_list))
    C = pars[scores.index(max(scores))]
    bestmodels = {
        "LinearSVC": LinearSVC(class_weight="balanced", C=C),
        "DecisionTree": DecisionTreeClassifier(max_depth=C),
        "RandomForest": RandomForestClassifier(max_features=C),
        "KNN": KNeighborsClassifier(n_neighbors=C),
        "NaiveBayes": MultinomialNB(alpha=C)
    }
    CVscores = max(scores)
    #Find Best model
    Best_clf = OneVsRestClassifier(bestmodels[alg])
    Best_train_clf = Best_clf.fit(X_train, Y_train)
    filename = os.path.join(cpath, 'models', 'method1', '%s_model.sav' % (alg))
    pickle.dump(Best_train_clf, open(filename, 'wb'))

    return CVscores


for x in alg:
    pars_item = pars[x]
    if x in ["LinearSVC", "NaiveBayes"]:
        pars_item = [float(s) for s in pars_item]
    else:
        pars_item = [int(s) for s in pars_item]

    CVscores = Train_Model(alg=x, pars=pars_item)
    filename = os.path.join(cpath, 'models', 'method1', '%s_CVscores.pkl' % (x))
    pickle.dump(CVscores, open(filename, 'wb'))
