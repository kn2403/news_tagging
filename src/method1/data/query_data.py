import pymongo
from pymongo import MongoClient
import json
import pprint
from bson.objectid import ObjectId
import os

client = MongoClient('mongodb://localhost:27017/')
db = client['scrapy_data']
collection = db['english_corpus']
data = collection.find(
    {}, {"title": 1,
         "description": 1,
         "text": 1,
         "tags": 1,
         "topic": 1}).limit(100)
jdata = []
for d in data:
    entry = {
        'title': d['title'],
        'description': d['description'],
        'text': d['text'],
        'tags': d['tags'],
        'topic': d['topic']
    }
    jdata.append(entry)

cpath = os.getcwd()
mongo_path = os.path.join(cpath, 'data', 'raw', 'mongodb_data.json')

with open(mongo_path, 'w') as f:
    json.dump(jdata, f)
