import os
import sklearn
import numpy as np
from sklearn.metrics import coverage_error, label_ranking_average_precision_score, label_ranking_loss
import pickle
import argparse
from beautifultable import BeautifulTable

#exist_models
cpath = os.getcwd()
path = os.path.join(cpath, 'models', 'method2')
exist_models = []


def uniq(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


for file in os.listdir(path):
    if file.endswith(".sav"):
        exist_models.append(file.split('_')[1])

parser = argparse.ArgumentParser()
parser.add_argument('--LinearSVC', action='store_true')
parser.add_argument('--DecisionTree', action='store_true')
parser.add_argument('--RandomForest', action='store_true')
parser.add_argument('--KNN', action='store_true')

input_args = vars(parser.parse_args())
pars = dict((k, v) for k, v in input_args.items() if v == True)
alg = list(pars.keys())

if alg == []:
    alg = uniq(exist_models)
elif any(a not in exist_models for a in alg):
    print('Please train the model first!')
    exit()


#test function: arg as a str
def Test_Model(arg, topic, X_test, Y_test, cpath=cpath):

    model_path = os.path.join(cpath, 'models', 'method2',
                              '%s_%s_model.sav' % (topic, arg))
    loaded_model = pickle.load(open(model_path, 'rb'))

    scores_path = os.path.join(cpath, 'models', 'method2',
                               '%s_%s_CVscores.pkl' % (topic, arg))
    CVscores = pickle.load(open(scores_path, 'rb'))

    Testscores = loaded_model.score(X_test, Y_test)
    predicted = loaded_model.predict(X_test)
    cov_error = coverage_error(Y_test, predicted)
    label_error = label_ranking_average_precision_score(Y_test, predicted)
    rank_error = label_ranking_loss(Y_test, predicted)

    results = [arg, CVscores, Testscores, cov_error, label_error, rank_error]

    return results


#Load test data
for topic in ['economy', 'politics', 'technology', 'sports']:

    tag_path = os.path.join(cpath, 'data', 'processed',
                            '%s_tags_test.txt' % (topic))
    Y_test = np.loadtxt(tag_path)
    text_path = os.path.join(cpath, 'data', 'processed',
                             '%s_features_test.txt' % (topic))
    X_test = np.loadtxt(text_path)

    table = BeautifulTable()
    table.column_headers = [
        '%s Algorithms' % (topic), 'CV scores', 'Test scores', 'Coverge error',
        'Label ranking average precision score', 'Label ranking loss'
    ]
    #Get results

    for a in alg:
        r = Test_Model(arg=a, topic=topic, X_test=X_test, Y_test=Y_test)

        table.append_row(r)

    print(table)
