import os
import pickle
import textacy
import numpy as np
import spacy
import scipy

#according the test_tag_model output, we found
best_choice = {
    'economy': 'KNN',
    'sports': 'KNN',
    'technology': 'KNN',
    'politics': 'DecisionTree'
}
cpath = os.getcwd()


#Target is single input
def label_detect(X_test,
                 topic,
                 text_clean,
                 best_choice=best_choice,
                 cpath=cpath):

    class_path = os.path.join(cpath, 'data', 'interim',
                              '%s_classes.pkl' % (topic))
    classes = pickle.load(open(class_path, 'rb'))

    model_path = os.path.join(cpath, 'models', 'method2',
                              '%s_%s_model.sav' % (topic, best_choice[topic]))

    model = pickle.load(open(model_path, 'rb'))
    probas = model.predict_proba(np.array([X_test]))

    inds = (probas > 0.7).astype(int)

    labels = classes.inverse_transform(inds)[0]
    labels = list(labels)

    if len(labels) <= 2:

        text = textacy.Doc(text_clean, lang='en')
        tag = textacy.keyterms.textrank(text, n_keyterms=3 - len(labels))

        new = [x[0] for x in tag]
        labels = labels + new

    return labels


features_path = os.path.join(cpath, 'data', 'processed', 'features_test.txt')
X_test = np.loadtxt(features_path)

text_path = os.path.join(cpath, 'data', 'interim', 'test_clean_text.txt')
text_clean = []

with open(text_path, "r") as f:
    for line in f.readlines():
        text_clean.append(line.strip())

tags_path = os.path.join(cpath, 'data', 'interim', 'test_clean_tags.txt')
tags_clean = []
with open(tags_path, "r") as f:
    for line in f.readlines():
        tags_clean.append(line.strip().split(','))

model_path = os.path.join(cpath, 'models', 'topic_model.sav')
model = pickle.load(open(model_path, 'rb'))
prediction = model.predict(X_test)
topics = ['economy', 'politics', 'technology', 'sports']
for feature, topic, text, tag in zip(X_test, prediction, text_clean,
                                     tags_clean):
    topic = topics[topic]
    label = label_detect(topic=topic, X_test=feature, text_clean=text)
    print('%s original:%s =====> predicted:%s\n' % (topic, tag, label))
