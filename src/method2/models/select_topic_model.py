import sklearn
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import KFold, cross_val_score
from scipy import io
import os
from scipy.sparse import vstack, coo_matrix
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.preprocessing import LabelEncoder
import statistics as s
import pickle
import scipy

cpath = os.getcwd()
X_train_all = []
Y_train_topics = []
X_test_all = []
Y_test_topics = []

i = 0
for topic in ['economy', 'politics', 'technology', 'sports']:
    train_path = os.path.join(cpath, 'data', 'processed',
                             '%s_features_train.txt' % (topic))
    train = np.loadtxt(train_path)

    test_path = os.path.join(cpath, 'data', 'processed',
                             '%s_features_test.txt' % (topic))
    test = np.loadtxt(test_path)


    X_train_all = X_train_all + train.tolist()
    X_test_all = X_test_all + test.tolist()
    Y_train_topics = Y_train_topics + [i] * len(train)
    Y_test_topics = Y_test_topics + [i] * len(test)
    i = i + 1

Y_train_topics = np.array(Y_train_topics)
Y_test_topics = np.array(Y_test_topics)
X_train_all = np.array(X_train_all)
X_test_all = np.array(X_test_all)
features_path = os.path.join(cpath, 'data', 'processed', 'features_test.txt')
np.savetxt(features_path, X_test_all)


svc_par = [str(x) for x in np.logspace(-6, -1, 10).tolist()]
pars = {
    "LinearSVC": svc_par,
    "DecisionTree": [1, 2, 3, 4, 5, 6, 7, 8],
    "RandomForest": [10, 15, 20, 25, 30],
    "KNN": [2, 3, 5, 7, 10],
    "NaiveBayes": [0.2, 0.5, 0.7, 1]
}
algs = list(pars.keys())


def Train_Model(alg,
                pars,
                X_train=X_train_all,
                Y_train=Y_train_topics,
                cpath=cpath):

    scores = []
    #Cross Validations for the selected model
    for c in pars:
        models = {
            "LinearSVC": LinearSVC(class_weight="balanced", C=c),
            "DecisionTree": DecisionTreeClassifier(max_depth=c),
            "RandomForest": RandomForestClassifier(max_features=c),
            "KNN": KNeighborsClassifier(n_neighbors=c),
            "NaiveBayes": MultinomialNB(alpha=c)
        }
        classifier = models[alg]
        k_fold = KFold(n_splits=5)
        scores_list = [
            classifier.fit(X_train[train], Y_train[train]).score(
                X_train[test], Y_train[test])
            for train, test in k_fold.split(X_train)
        ]
        scores.append(s.mean(scores_list))
    C = pars[scores.index(max(scores))]
    bestmodels = {
        "LinearSVC": LinearSVC(class_weight="balanced", C=C),
        "DecisionTree": DecisionTreeClassifier(max_depth=C),
        "RandomForest": RandomForestClassifier(max_features=C),
        "KNN": KNeighborsClassifier(n_neighbors=C),
        "NaiveBayes": MultinomialNB(alpha=C)
    }
    CVscores = max(scores)
    #Find Best model
    Best_clf = bestmodels[alg]
    Best_train_clf = Best_clf.fit(X_train, Y_train)

    return Best_train_clf


all_models = {}
accuracies = {}
for alg in algs[1:-1]:
    model = Train_Model(alg=alg, pars=pars[alg])
    prediction = model.predict(X_test_all)
    accuracy = [x == y for x, y in zip(prediction, Y_test_topics)]
    all_models[alg] = model
    accuracies[alg] = sum(accuracy) / len(accuracy)
    print('%s accuracy rate: %s' % (alg, accuracies[alg]))

key = max(accuracies, key=accuracies.get)
filename = os.path.join(cpath, 'models', 'topic_model.sav')
pickle.dump(all_models[key], open(filename, 'wb'))
