import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer
from pprint import pprint
from nltk.stem.snowball import SnowballStemmer
import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.decomposition import TruncatedSVD
import os
import numpy as np
import pickle

#Extract features from text
cpath = os.getcwd()
text_clean_all = []
text_clean_test = []
length = {}
test = {}
for topic in ['economy', 'politics', 'technology', 'sports']:
    text_path = os.path.join(cpath, 'data', 'interim',
                             '%s_clean_text.txt' % (topic))
    text_clean = []
    with open(text_path, "r") as f:

        for line in f.readlines():
            text_clean.append(line.strip())

    n = int(len(text_clean) * 0.2)
    test[topic] = n
    length[topic] = len(text_clean)
    text_clean_all = text_clean_all + text_clean 
    text_clean_test = text_clean_test + text_clean[-n:]
#for new prediction
text_path = os.path.join(cpath, 'data', 'interim', 'test_clean_text.txt')
with open(text_path, "w") as f:
    for text in text_clean_test:
        f.write(text + '\n')

count_vect = CountVectorizer()
X_counts = count_vect.fit_transform(text_clean_all)
tfidf_transformer = TfidfTransformer()
X_tfidf = tfidf_transformer.fit_transform(X_counts)
svd = TruncatedSVD(n_components=200)
svd.fit(X_tfidf)
X_tfidf = svd.transform(X_tfidf)

accum = 0
tags_test = []
for topic in ['economy', 'politics', 'technology', 'sports']:
    X = X_tfidf[accum:length[topic] + accum]
    accum = accum + length[topic]
    features_path1 = os.path.join(cpath, 'data', 'processed',
                                  '%s_features_train.txt' % (topic))
    features_path2 = os.path.join(cpath, 'data', 'processed',
                                  '%s_features_test.txt' % (topic))
    np.savetxt(features_path1, X[:-test[topic]])
    np.savetxt(features_path2, X[-test[topic]:])

    #Extract features from tags
    tags_path = os.path.join(cpath, 'data', 'interim',
                             '%s_clean_tags.txt' % (topic))
    tags_clean = []
    with open(tags_path, "r") as f:

        for line in f.readlines():
            tags_clean.append(line.strip().split(','))
    tags_test = tags_test + tags_clean[-test[topic]:]

    mlb = MultiLabelBinarizer()
    tags = mlb.fit_transform(tags_clean)
    class_path = os.path.join(cpath, 'data', 'interim',
                              '%s_classes.pkl' % (topic))
    pickle.dump(mlb, open(class_path, 'wb'))
    n = test[topic]

    #save the tags
    tag_path1 = os.path.join(cpath, 'data', 'processed',
                             '%s_tags_train.txt' % (topic))
    np.savetxt(tag_path1, tags[:-n])
    tag_path2 = os.path.join(cpath, 'data', 'processed',
                             '%s_tags_test.txt' % (topic))
    np.savetxt(tag_path2, tags[-n:])

tag_path = os.path.join(cpath, 'data', 'interim', 'test_clean_tags.txt')

with open(tag_path, "w") as f:
    for tag in tags_test:
        f.write(str(','.join(tag)) + '\n')
