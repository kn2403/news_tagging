import pymongo
from pymongo import MongoClient
import json
import pprint
from bson.objectid import ObjectId
import os

for topic in ['economy', 'politics', 'technology', 'sports']:
    client = MongoClient('mongodb://localhost:27017/')
    db = client['scrapy_data']
    collection = db['english_corpus']
    data = collection.find({
        'topic': topic
    }, {"title": 1,
        "description": 1,
        "text": 1,
        "tags": 1}).limit(2000)
    jdata = []
    for d in data:
        entry = {
            'title': d['title'],
            'description': d['description'],
            'text': d['text'],
            'tags': d['tags']
        }
        jdata.append(entry)

    cpath = os.getcwd()
    mongo_path = os.path.join(cpath, 'data', 'raw', '%s_data.json' % topic)

    with open(mongo_path, 'w') as f:
        json.dump(jdata, f)
