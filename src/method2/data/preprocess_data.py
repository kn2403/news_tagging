import json
import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer
from nltk.stem.snowball import SnowballStemmer
import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import re
import os
cpath = os.getcwd()


def preprocess(item):
    #lower the case
    item_lower = item.lower()

    #Stem the words
    stemmer = SnowballStemmer("english")
    item_stem = stemmer.stem(item_lower)

    #Remove space, punctuation and stop words

    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(item_stem)
    filtered_words = [w for w in tokens if not w in stopwords.words('english')]
    join_words = " ".join(filtered_words)

    return re.sub(r'\w*\d\w*', '', join_words).strip()


def preprocess_tag(item):
    #lower the case
    item_lower = item.lower()

    #Remove space, punctuation and stop words

    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(item_lower)
    join_words = " ".join(tokens)

    return re.sub(r'\w*\d\w*', '', join_words).strip()


for topic in ['economy', 'politics', 'technology', 'sports']:

    mongo_path = os.path.join(cpath, 'data', 'raw', '%s_data.json' % (topic))
    with open(mongo_path) as json_data:
        d = json.load(json_data)
    df = pd.DataFrame.from_dict(d, orient='columns')
    rawtags = df.tags

    #process tags
    cleantags = []
    for tag in rawtags:
        cleantag = [preprocess_tag(x) for x in tag]
        cleantags.append(cleantag)

    mlb = MultiLabelBinarizer()
    tags = mlb.fit_transform(cleantags)
    classes = list(mlb.classes_)
    tags = [sum(tags[:, i]) for i in range(len(classes))]

    dic = {}
    for classe, tag in zip(classes, tags):
        dic[classe] = int(tag)

    toptag = []
    for tag in cleantags:

        ntag = sorted(tag, key=lambda x: dic[x])
        tag = ntag[-3:]
        toptag.append(tag)

    tag_path = os.path.join(cpath, 'data', 'interim',
                            '%s_clean_tags.txt' % (topic))

    with open(tag_path, "w") as f:
        for tag in toptag:
            f.write(str(','.join(tag)) + '\n')

#Process features
    combo = [' '.join([x, y]) for x, y in zip(df.text, df.title)]

    processed_text = [preprocess(x) for x in combo]

    text_path = os.path.join(cpath, 'data', 'interim',
                             '%s_clean_text.txt' % (topic))
    with open(text_path, "w") as f:
        for text in processed_text:
            f.write(text + '\n')
