﻿news_tagging
==============================

This is a project to detect the labels for text contents, by comparing models including KNN, LinearSVC, NaiveBayes, DecisionTrees and RandomForest.

The notebook runs the full process without customized commands, which runs the five models with default parameters. User can customize parameters to tune and models to use by running python train_model.py --[model name] [par1] [par2] [par3] ..
User can also choose models to test if the model has been trained by running: python predict_model.py --[model name]


Here is an example for running the project step by step：


* run src/[method]/data/query_data.py, this file downloads data from mongoDB and stores them in data/raw, for method 2,

* run src/[method]/data/preprocess_data.py , this file cleans the data file inside data/raw and saves it inside         data/interim

* run src/[method]/features/build_features.py, which extracts features for clean data and saves them as test and         training data inside the data/processed

* run src/[method]/models/train_tag_model.py --[model 1] [par1] [par2] ... --[model 2] [par1] [par2] [par3]...
  The available choices for models are LinearSVC, SVM, DecisionTree, RandomForest and NaiveBayes, also for each model        selected, user can input tuning variables after that. The trained models will be saved inside models/method

* run src/[method]/models/test_tag_model.py --[model1] --[model2] ...
  The available choices are trained models. If user selects an untrained model, program will tell user to change and         exit. For method 1, the results list will be saved in data/interim, for method 2, it displays a result table for each      topic, user should decide which one is the best.

* (ONLY method 1) to print the results, user can use the last cell in notebooks/main_method1.ipynb to display them in        BeautifulTable

* (ONLY method 2) run src/method2/models/select_topic_model.py to train and select the best model for topic detecting.

* (ONLY method 2) run src/method2/models/predict_new_labels.py to get predicted labels for test data. First the topic   model will detect the topic of the new data. A model decided by new data's topic will detect the tags of the data. If   the model returns less than three tags, the text rank method will return the rest of predicted tags.




Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
